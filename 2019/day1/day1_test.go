package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRequiredFuelPart1(t *testing.T) {

	assert.Equal(t, RequiredFuelPart1(12), 2, "should be equal")
	assert.Equal(t, RequiredFuelPart1(14), 2, "should be equal")
	assert.Equal(t, RequiredFuelPart1(1969), 654, "should be equal")
	assert.Equal(t, RequiredFuelPart1(100756), 33583, "should be equal")
}

func TestRequiredFuelPart2(t *testing.T) {

	assert.Equal(t, RequiredFuel(2, 0), 0, "should be equal")
	assert.Equal(t, RequiredFuel(12, 0), 2, "should be equal")
	assert.Equal(t, RequiredFuel(14, 0), 2, "should be equal")
	assert.Equal(t, RequiredFuel(1969, 0), 966, "should be equal")
	assert.Equal(t, RequiredFuel(100756, 0), 50346, "should be equal")
}

