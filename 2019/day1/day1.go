package main

import (
	"fmt"
	"gitlab.com/dopsonbr/advent-of-code/pkg/util"
	"math"
)

//The Elves quickly load you into a spacecraft and prepare to launch.
//
//At the first Go / No Go poll, every Elf is Go until the Fuel Counter-Upper. They haven't determined the amount of fuel required yet.
//
//Fuel required to launch a given module is based on its mass. Specifically, to find the fuel required for a module, take its mass, divide by three, round down, and subtract 2.
//
//For example:
//
//For a mass of 12, divide by 3 and round down to get 4, then subtract 2 to get 2.
//For a mass of 14, dividing by 3 and rounding down still yields 4, so the fuel required is also 2.
//For a mass of 1969, the fuel required is 654.
//For a mass of 100756, the fuel required is 33583.
//The Fuel Counter-Upper needs to know the total fuel requirement.
//To find it, individually calculate the fuel needed for the mass of each module (your puzzle input),
//then add together all the fuel values.
//
//What is the sum of the fuel requirements for all of the modules on your spacecraft?

func RequiredFuelPart1(mass int, fuel int) int {
	s1 := float64(mass / 3)
	asInt := int(math.Trunc(s1))
	return asInt - 2
}

func RequiredFuel(mass int, fuel int) int {
	rf := int(math.Trunc(float64(mass/3))) - 2
	if rf < 0 {
		return fuel
	} else {
		return RequiredFuel(rf, rf+fuel)
	}
}

func part1() {
	res, _ := util.InputHandler("2019/day1/day1_input.txt", RequiredFuelPart1, 0)
	fmt.Printf("part 1: %s\n", res)
}

func part2() {
	res, _ := util.InputHandler("2019/day1/day1_input.txt", RequiredFuel, 0)
	fmt.Printf("part 2: %s\n", res)
}

func main() {
	part1()
	part2()
}
