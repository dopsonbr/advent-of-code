package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPart1(t *testing.T) {
	assert.Equal(t, []int{2, 0, 0, 0, 99}, part1([]int{1, 0, 0, 0, 99}, 0), "should be equal")
	assert.Equal(t, []int{2, 3, 0, 6, 99}, part1([]int{2, 3, 0, 3, 99}, 0), "should be equal")
	assert.Equal(t, []int{2, 4, 4, 5, 99, 9801}, part1([]int{2, 4, 4, 5, 99, 0}, 0), "should be equal")
	assert.Equal(t, []int{30, 1, 1, 4, 2, 5, 6, 0, 99}, part1([]int{1, 1, 1, 4, 99, 5, 6, 0, 99}, 0), "should be equal")
	//assert.Equal(t, part1([]int{1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50}), 2, "should be equal")
}
