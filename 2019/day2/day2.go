package main

import (
	"errors"
	"fmt"
)

func part1(input []int, idx int) []int {
	switch input[idx] {
	case 1:
		return part1(operate(input, idx, add), idx+4)
	case 2:
		return part1(operate(input, idx, mult), idx+4)
	default:
		return input
	}
}

func mult(x int, y int) int {
	return x * y
}

func add(x int, y int) int {
	return x + y
}

func operate(input []int, idx int, operation func(int, int) int) []int {
	x := input[input[idx+1]]
	y := input[input[idx+2]]
	res := operation(x, y)
	pos := input[idx+3]
	//fmt.Printf("idx: %d, x: %d, y: %d res: %d, pos: %d\n", idx, x, y, res, pos)
	input[pos] = res
	return input
}

func check(copied []int, res []int) error {
	for i, v := range copied {
		if res[i] == v {
			return  errors.New(fmt.Sprintf("should not be equal \n %v\n %v\n", copied, res))
		}
	}
	return nil
}

func part2(want int, input []int) ([]int, error) {

	copied := make([]int, len(input))
	copy(copied, input)

	for i :=0; i<100; i++ {
		for j :=0; j<100; j++ {
			copy(copied, input)
			copied[1] = i
			copied[2] = j
			res := part1(copied, 0)
			//err := check(copied, res)
			//if err != nil {
			//	return nil, err
			//}
			//fmt.Printf("input: %v\n copied: %v\n res: %v\n", input, copied, res)
			found := res[0]
			fmt.Printf("found: %d, i: %d, j%d\n", res[0], i, j)
			if want == found {
				return res, nil
			}

		}
	}
	fmt.Print("unable to find desired value. returning input")
	return input, nil

	//return part2(want, input, i+1, j+1)
	//return part2(want, input, 0, i++, j++)
}

func main() {
	input := []int{1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,13,1,19,1,9,19,23,2,23,13,27,1,27,9,31,2,31,6,35,1,5,35,39,1,10,39,43,2,43,6,47,1,10,47,51,2,6,51,55,1,5,55,59,1,59,9,63,1,13,63,67,2,6,67,71,1,5,71,75,2,6,75,79,2,79,6,83,1,13,83,87,1,9,87,91,1,9,91,95,1,5,95,99,1,5,99,103,2,13,103,107,1,6,107,111,1,9,111,115,2,6,115,119,1,13,119,123,1,123,6,127,1,127,5,131,2,10,131,135,2,135,10,139,1,13,139,143,1,10,143,147,1,2,147,151,1,6,151,0,99,2,14,0,0}

	input[1], input[2] = 12, 2

	p1Input := make([]int, len(input))
	p2Input := make([]int, len(input))
	copy(p1Input, input)
	copy(p2Input, input)

	fmt.Printf("part 1: %v\n",  part1(p1Input, 0))

	res, err := part2(19690720, input)
	fmt.Printf("part 2: \n %v\n %v", res, err)

}

//
//--- Day 2: 1202 Program Alarm ---
//On the way to your gravity assist around the Moon, your ship computer beeps angrily about a "1202 program alarm". On the radio, an Elf is already explaining how to handle the situation: "Don't worry, that's perfectly norma--" The ship computer bursts into flames.
//
//You notify the Elves that the computer's magic smoke seems to have escaped. "That computer ran Intcode programs like the gravity assist program it was working on; surely there are enough spare parts up there to build a new Intcode computer!"
//
//An Intcode program is a list of integers separated by commas (like 1,0,0,3,99). To run one, start by looking at the first integer (called position 0). Here, you will find an opcode - either 1, 2, or 99. The opcode indicates what to do; for example, 99 means that the program is finished and should immediately halt. Encountering an unknown opcode means something went wrong.
//
//Opcode 1 adds together numbers read from two positions and stores the result in a third position. The three integers immediately after the opcode tell you these three positions - the first two indicate the positions from which you should read the input values, and the third indicates the position at which the output should be stored.
//
//For example, if your Intcode computer encounters 1,10,20,30, it should read the values at positions 10 and 20, add those values, and then overwrite the value at position 30 with their sum.
//
//Opcode 2 works exactly like opcode 1, except it multiplies the two inputs instead of adding them. Again, the three integers after the opcode indicate where the inputs and outputs are, not their values.
//
//Once you're done processing an opcode, move to the next one by stepping forward 4 positions.
//
//For example, suppose you have the following program:
//
//1,9,10,3,2,3,11,0,99,30,40,50
//For the purposes of illustration, here is the same program split into multiple lines:
//
//1,9,10,3,
//2,3,11,0,
//99,
//30,40,50
//The first four integers, 1,9,10,3, are at positions 0, 1, 2, and 3. Together, they represent the first opcode (1, addition), the positions of the two inputs (9 and 10), and the position of the output (3). To handle this opcode, you first need to get the values at the input positions: position 9 contains 30, and position 10 contains 40. Add these numbers together to get 70. Then, store this value at the output position; here, the output position (3) is at position 3, so it overwrites itself. Afterward, the program looks like this:
//
//1,9,10,70,
//2,3,11,0,
//99,
//30,40,50
//Step forward 4 positions to reach the next opcode, 2. This opcode works just like the previous, but it multiplies instead of adding. The inputs are at positions 3 and 11; these positions contain 70 and 50 respectively. Multiplying these produces 3500; this is stored at position 0:
//
//3500,9,10,70,
//2,3,11,0,
//99,
//30,40,50
//Stepping forward 4 more positions arrives at opcode 99, halting the program.
//
//Here are the initial and final states of a few more small programs:
//
//1,0,0,0,99 becomes 2,0,0,0,99 (1 + 1 = 2).
//2,3,0,3,99 becomes 2,3,0,6,99 (3 * 2 = 6).
//2,4,4,5,99,0 becomes 2,4,4,5,99,9801 (99 * 99 = 9801).
//1,1,1,4,99,5,6,0,99 becomes 30,1,1,4,2,5,6,0,99.
//Once you have a working computer, the first step is to restore the gravity assist program (your puzzle input) to the "1202 program alarm" state it had just before the last computer caught fire. To do this, before running the program, replace position 1 with the value 12 and replace position 2 with the value 2. What value is left at position 0 after the program halts?
