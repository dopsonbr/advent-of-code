package util

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func InputHandler(fileName string, solver func(a int, b int) int, initialValue int) (string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	sum := initialValue
	for scanner.Scan() {
		mass, _ := strconv.Atoi(scanner.Text())
		sum = solver(mass, initialValue) + sum
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return strconv.Itoa(sum), nil
}

